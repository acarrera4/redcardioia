from django import forms
from django.forms.widgets import CheckboxInput, RadioSelect

from .models import Fle

import datetime

from django.utils.safestring import mark_safe



procedure_CHOICES = [
        ('1','Vias Altas Digestivas'), 
        ('2',' '), 
        ('3',' '), 
        ('4','Otro')
]

AFF_CHOICES=[
        ('','Seleccionar'),
        ('1','Si'),
        ('0','No'),
        ]

Af_CHOICES=[
        ('1','Si'),
        ('0','No'),
        ]

class FleForm(forms.ModelForm):
    day_procedure       = forms.DateField(required=False,initial=datetime.date.today,
                    widget=forms.DateInput(attrs={"class": "form-control",'type':'date', 'id':'date_procedure'
                                }))
    day_uploaded = forms.DateField(initial=datetime.date.today, widget=forms.DateInput(attrs={'class':'form-control','id':'day_uploaded'}))  

    procedure = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'procedure_select','name': 'procedure_select', 'id': 'procedure_select'})) 
    procedure_other = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'procedure_other','name': 'procedure_other', 'id': 'procedure_other'})) 
    exam_diagnosis = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'diagn_select','name': 'diagn_select', 'id': 'diagn_select'})) 
    exam_diagnosis_explain = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'diagn_other','name': 'diagn_other', 'id': 'diagn_other'})) 
    biopsy = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'biopsy_select','name': 'biopsy_select', 'id': 'biopsy_select'})) 
    biopsy_diagnosis = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'biopsy_diagnosis','name': 'biopsy_diagnosis', 'id': 'biopsy_diagnosis'})) 
    biopsy_analysis = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'biopsy_analysis','name': 'biopsy_analysis', 'id': 'biopsy_analysis'})) 
    indication = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'indication_select','name': 'indication_select', 'id': 'indication_select'})) 
    indication_desc = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'indication_other','name': 'indication_other', 'id': 'indication_other'})) 
    antecedent = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'antecedent_select', 'name': 'antecedent_select', 'id': 'antecedent_select'}))
    antecedent_desc = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'ant_desc','name': 'ant_desc', 'id': 'ant_desc'}))  
    antecedent_helicob = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'antecedent_helicob_select','name': 'antecedent_helicob_select', 'id': 'antecedent_helicob_select'}))  
    
    antecedent_ulcera   = forms.ChoiceField( required=True, widget=RadioSelect(attrs={'id': 'antecedent_ulcera_select','class': 'form-check-inline antecedent_ulcera_select', 'type':'radio', 'style': 'display: inline-block; padding: 0; margin: 0; margin-left: 5px'}), choices=Af_CHOICES,initial='0')  
    antecedent_AINES= forms.ChoiceField( required=True, widget=RadioSelect(attrs={'id': 'antecedent_AINES_select','class': 'form-check-inline antecedent_AINES_select', 'type':'radio', 'style': 'display: inline-block; padding: 0; margin: 0; margin-left: 5px'}), choices=Af_CHOICES,initial='0')  
    antecedent_IBP = forms.ChoiceField( required=True, widget=RadioSelect(attrs={'id': 'antecedent_IBP_select','class': 'form-check-inline antecedent_IBP_select', 'type':'radio', 'style': 'display: inline-block; padding: 0; margin: 0; margin-left: 5px'}), choices=Af_CHOICES,initial='0')  
    antecedent_antib = forms.ChoiceField( required=True, widget=RadioSelect(attrs={'id': 'antecedent_antib_select','class': 'form-check-inline antecedent_antib_select', 'type':'radio', 'style': 'display: inline-block; padding: 0; margin: 0; margin-left: 5px'}), choices=Af_CHOICES,initial='0')  
    informed_cons = forms.ChoiceField( required=True, widget=RadioSelect(attrs={'id': 'informed_cons','class': 'form-check-inline informed_cons', 'type':'radio', 'style': 'display: inline-block; padding: 0; margin: 0; margin-left: 5px'}), choices=Af_CHOICES,initial='0')    
          
    class Meta:
        model = Fle
        fields = ('title',
                'root_fle',
                'active_flg_file',
                'fle',
                'type_file',
                'img_file',
                'day_procedure',
                'day_uploaded',
                'id_pateint',
                'procedure',
                'procedure_other',
                'exam_diagnosis',
                'exam_diagnosis_explain',
                'biopsy',
                'biopsy_diagnosis',
                'biopsy_analysis',
                'indication',
                'indication_desc',
                'informed_cons',
                'antecedent',
                'antecedent_desc',
                'antecedent_helicob',
                'antecedent_ulcera',
                'antecedent_AINES',
                'antecedent_IBP',
                'antecedent_antib',
                'num_anot',
                'report',
                'user_edit',
                'root_inf_cons',
                'flg_inf_cons')

    def __init__(self, *args, **kwargs):
        super(FleForm, self).__init__(*args, **kwargs)
        self.fields['report'].required = False
        self.fields['fle'].required = False


